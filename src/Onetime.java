import java.time.LocalDate;

public class Onetime extends Appointment {

    //constructor
    public Onetime(String description, LocalDate date) {
        super(description, date);
    }

}