import java.io.*;
import java.sql.Array;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;


public class P9_3 {

    /**
     * Function Checks for any appoinments that occur on or after the date
     * specified by the user.
     *
     * @param apps
     */
    public static void checkAppointment(ArrayList<Appointment> apps) {
        int iYear, iMonth, iDay;
        Scanner keyboard = new Scanner(System.in);

        System.out.printf("Enter a year: ");
        iYear = keyboard.nextInt();

        System.out.printf("Enter a Month: ");
        iMonth = keyboard.nextInt();

        System.out.printf("Enter a Day: ");
        iDay = keyboard.nextInt();

        System.out.println("Here is a List of appointments after the date you have entered");
        for (int i = 0; i < apps.size(); i++) {
            if (apps.get(i).occursOn(iYear, iMonth, iDay))
                System.out.printf("You have a %s appointment on %s\n", apps.get(i).getStrDescription(), apps.get(i).getLocDate().toString());
        }
    }

    /**
     * This function adds a user defined Appointment to the array list in main
     *
     * @param myArray
     * @throws IOException
     */
    public static void addAppointment(ArrayList <Appointment> myArray) throws IOException {
        //ask user for type
        System.out.print("Enter the type (O - Onetime, D - Daily, or M - Monthly): ");
        Scanner input = new Scanner(System.in);
        String type = input.next();
        String description;
        String date;
        LocalDate sometime;
        //use different cases to create specific types of appointments.
        switch (type) {
            case "O" -> {
                System.out.printf("Enter the date (yyyy-mm-dd): ");
                sometime = LocalDate.parse(input.next());
                System.out.printf("Enter the description: ");
                description = input.next();
                Onetime someAppointment = new Onetime(description, sometime);
                myArray.add(someAppointment);
                save("appointments.txt", someAppointment);
            }
            case "D" -> {
                System.out.printf("Enter the date (yyyy-mm-dd): ");
                sometime = LocalDate.parse(input.next());
                System.out.printf("Enter the description: ");
                description = input.next();
                Daily anotherAppointment = new Daily(description, sometime);
                myArray.add(anotherAppointment);
                save("appointments.txt", anotherAppointment);
            }
            case "M" -> {
                System.out.printf("Enter the date (yyyy-mm-dd): ");
                sometime = LocalDate.parse(input.next());
                System.out.printf("Enter the description: ");
                description = input.next();
                Monthly anAppointment = new Monthly(description, sometime);
                myArray.add(anAppointment);
                save("appointments.txt", anAppointment);
            }
        }
    }

    /**
     * This function saves the Appointment passed by reference in the method to
     * a specific file that is also specified in the method parameters.
     * @param file
     * @param appointment
     * @throws IOException
     */
    public static void save(String file, Appointment appointment )throws IOException {
        FileWriter fwriter = new FileWriter(file, true);
        PrintWriter output  = new PrintWriter(fwriter);
        String type = "";
        if(appointment instanceof Daily) type = "Daily";
        else if(appointment instanceof Monthly) type = "Monthly";
        else if(appointment instanceof Onetime) type = "Onetime";
        output.println(type + " " + appointment.getStrDescription() + " " + appointment.getLocDate().toString());
        output.close();
    }

    /**
     * Loads the Appointments listed in a separate file into an ArrayList and returns the arrayList back to the caller
     * @param filename
     * @return the arrayList that contains the filled array
     * @throws FileNotFoundException
     */
    public static ArrayList<Appointment> load(String filename) throws FileNotFoundException{
        File myFile = new File(filename);
        Scanner inputFile = new Scanner(myFile);
        String type, description, line; //line used to scan for integer in the line
        LocalDate date;
        ArrayList<Appointment> appList = new ArrayList();
        int i;

        while(inputFile.hasNext()){ //until the end of file
            type = inputFile.next();
            //going to separate line from type and description and isolate the date
            i = 0;
            line = inputFile.nextLine();
            while(!Character.isDigit(line.charAt(i))){i++;}
            description = line.substring(0, i);                 //first part of the string
            date = LocalDate.parse(line.substring(i).trim());   //second part of the string


            //create the object to fill the arraylist
             Appointment temp = null;
             switch(type){
                 case "Onetime":
                     temp = new Onetime(description, date);
                     break;
                 case "Monthly":
                     temp = new Monthly(description, date);
                     break;
                 case "Daily":
                     temp = new Daily(description,date);
                     break;
             }

             appList.add(temp);
        }

        inputFile.close();
        return appList;
    }

    /**
     * Main function
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        ArrayList <Appointment> appList = new ArrayList<Appointment>();
        LocalDate dentist = LocalDate.of(2021, 10, 15);
        LocalDate schoolLecture = LocalDate.of(2021, 10, 15);
        LocalDate studentBodyMeeting = LocalDate.of(2021, 10, 15);
        Appointment appDentist = new Onetime("dentist", dentist);
        Appointment appSchool =  new Daily("School Lecture", schoolLecture);
        Appointment appStudentMeeting = new Monthly("Student Body Meeting", studentBodyMeeting);

        //given list of appointments
        appList.add(appDentist);
        save("appointments.txt",appDentist);
        appList.add(appSchool);
        save("appointments.txt", appSchool);
        appList.add(appStudentMeeting);
        save("appointments.txt", appStudentMeeting);
        ArrayList<Appointment> loadedList = new ArrayList<Appointment>();
        ArrayList<Appointment> checkList = new ArrayList<Appointment>();
        Scanner user = new Scanner(System.in);
        String choice;
        do{
            System.out.printf("Select a option: A for add an appointment, C for checking, L to load, Q to quit: ");
            choice = user.next();
            if(choice.equals("C"))                                  //checks to see what appointments occur on
                                                                    //user specified date
                checkAppointment(appList);
            else if(choice.equals("A")) {                            //Add to appoinments and save it to file
                addAppointment(appList);
                System.out.println("These are the new appointments that are saved please check for your newly added appointment!");
                checkList = load("appointments.txt");
                System.out.printf(checkList.toString());
            }
            else if(choice.equals("L")) {
                loadedList = load("savedApps.txt");         //load and print the loaded results
                System.out.println("This is the contents of the file you have loaded");
                System.out.println(loadedList.toString());
            }
        }while(choice.compareTo("Q") != 0);

    }
}
/**
 * Sample Output
 *
 * Select a option: A for add an appointment, C for checking, L to load, Q to quit: C]
 * Select a option: A for add an appointment, C for checking, L to load, Q to quit: C
 * Enter a year: 2021
 * Enter a Month: 11
 * Enter a Day: 15
 * Here is a List of appointments after the date you have entered
 * You have a School Lecture appointment on 2021-10-15
 * You have a Student Body Meeting appointment on 2021-10-15
 * Select a option: A for add an appointment, C for checking, L to load, Q to quit: C
 * Enter a year: 2021
 * Enter a Month: 9
 * Enter a Day: 15
 * Here is a List of appointments after the date you have entered
 * Select a option: A for add an appointment, C for checking, L to load, Q to quit: C
 * Enter a year: 2021
 * Enter a Month: 12
 * Enter a Day: 5
 * Here is a List of appointments after the date you have entered
 * You have a School Lecture appointment on 2021-10-15
 * Select a option: A for add an appointment, C for checking, L to load, Q to quit: L
 * This is the contents of the file you have loaded
 * [ Physics Office Hours 2021-05-10,  Church Service 2021-06-19,  Halloween 2021-10-31]
 * Select a option: A for add an appointment, C for checking, L to load, Q to quit: A
 * Enter the type (O - Onetime, D - Daily, or M - Monthly): M
 * Enter the date (yyyy-mm-dd): 2003-08-10
 * Enter the description: Doctor's
 * These are the new appointments that are saved please check for your newly added appointment!
 * [ dentist 2021-10-15
 * ,  School Lecture 2021-10-15
 * ,  Student Body Meeting 2021-10-15
 * ,  Doctor's 2003-08-10
 * ]
 * Select a option: A for add an appointment, C for checking, L to load, Q to quit: Q
 *
 *
 * Process finished with exit code 0
 */
