import java.time.LocalDate;

public class Monthly extends Appointment{
    public Monthly(String description, LocalDate date){
        super(description, date);
    }

    @Override
    public Boolean occursOn(int year, int month, int day) {
        if(getLocDate().getDayOfMonth() == day && getLocDate().isBefore(LocalDate.of(year, month, day-1)))
            return true;
        else
            return false;

    }
}
