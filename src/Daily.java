import java.time.LocalDate;

public class Daily extends Appointment{
    public Daily(String description, LocalDate date){
        super(description, date);
    }
    public Boolean occursOn(int year, int month, int day){
        if(this.getLocDate().isBefore(LocalDate.of(year, month, day-1)))
            return true;
        return false;
    }

}
