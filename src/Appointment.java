import java.time.LocalDate;

public class Appointment {
    private String strDescription;
    private LocalDate locDate;

    public Appointment(String descrip, LocalDate date){
        strDescription = descrip;
        locDate = date;
    }

    // Getter Methods

    public String getStrDescription() {
        return strDescription;
    }

    public LocalDate getLocDate() {
        return locDate;
    }
    public Boolean occursOn(int year, int month, int day){
        boolean equals = locDate.equals(LocalDate.of(year, month, day));
        if(equals) return true;
        return false;
    }

    @Override
    public String toString(){
        return (this.getStrDescription() + this.getLocDate().toString() + "\n" );
    }
}
